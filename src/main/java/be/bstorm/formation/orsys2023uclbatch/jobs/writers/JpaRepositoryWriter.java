package be.bstorm.formation.orsys2023uclbatch.jobs.writers;

import org.springframework.batch.item.Chunk;
import org.springframework.batch.item.ItemWriter;
import org.springframework.data.jpa.repository.JpaRepository;

public class JpaRepositoryWriter<TEntity, TRepo extends JpaRepository<TEntity, Long>> implements ItemWriter<TEntity> {
    private final TRepo repo;

    public JpaRepositoryWriter(TRepo repo) {
        this.repo = repo;
    }

    @Override
    public void write(Chunk<? extends TEntity> chunk) throws Exception {
        repo.saveAll(chunk.getItems());
    }
}
