package be.bstorm.formation.orsys2023uclbatch.repositories;

import be.bstorm.formation.orsys2023uclbatch.models.entities.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
}
