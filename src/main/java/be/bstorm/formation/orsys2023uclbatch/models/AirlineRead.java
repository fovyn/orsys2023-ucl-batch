package be.bstorm.formation.orsys2023uclbatch.models;

import lombok.Data;

@Data
public class AirlineRead {
    private Long AirlineID;
    private String AirlineName;
    private String Alias;
    private String IATACode;
    private String ICAOCode;
    private String CallSign;
    private String Country;
    private String Active;
}
