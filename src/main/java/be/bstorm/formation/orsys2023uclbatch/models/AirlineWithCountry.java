package be.bstorm.formation.orsys2023uclbatch.models;

import be.bstorm.formation.orsys2023uclbatch.models.entities.Airline;
import be.bstorm.formation.orsys2023uclbatch.models.entities.Country;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AirlineWithCountry {
    private Airline airline;
    private Country country;
}
