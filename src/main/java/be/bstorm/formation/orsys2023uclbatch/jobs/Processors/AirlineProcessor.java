package be.bstorm.formation.orsys2023uclbatch.jobs.Processors;

import be.bstorm.formation.orsys2023uclbatch.models.AirlineRead;
import be.bstorm.formation.orsys2023uclbatch.models.AirlineWithCountry;
import be.bstorm.formation.orsys2023uclbatch.models.entities.Airline;
import be.bstorm.formation.orsys2023uclbatch.models.entities.Country;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;

@Slf4j
public class AirlineProcessor implements ItemProcessor<AirlineRead, AirlineWithCountry> {

    @Override
    public AirlineWithCountry process(AirlineRead item) throws Exception {
        Country country = Country.builder().name(item.getCountry()).build();
        Airline airline = Airline.builder()
                .id(item.getAirlineID())
                .isActive(item.getActive().equals("Y"))
                .alias(item.getAlias())
                .IATACode(item.getIATACode())
                .name(item.getAirlineName())
                .build();


        log.info("Airline => {}", airline);
        log.info("Country => {}", country);

        return AirlineWithCountry.builder()
                .airline(airline)
                .country(country)
                .build();
    }
}
