package be.bstorm.formation.orsys2023uclbatch.repositories;

import be.bstorm.formation.orsys2023uclbatch.models.entities.Airline;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AirlineRepository extends JpaRepository<Airline, Long> {
}
