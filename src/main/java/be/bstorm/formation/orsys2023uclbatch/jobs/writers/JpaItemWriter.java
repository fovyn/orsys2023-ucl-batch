package be.bstorm.formation.orsys2023uclbatch.jobs.writers;

import lombok.RequiredArgsConstructor;
import org.springframework.batch.item.Chunk;
import org.springframework.batch.item.ItemWriter;

import java.util.function.Consumer;

@RequiredArgsConstructor
public class JpaItemWriter<T> implements ItemWriter<T> {
    private final Consumer<T> action;

    @Override
    public void write(Chunk<? extends T> chunk) throws Exception {
        chunk.getItems().forEach(action);
    }
}
