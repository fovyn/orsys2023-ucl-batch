package be.bstorm.formation.orsys2023uclbatch.jobs;

import be.bstorm.formation.orsys2023uclbatch.jobs.Processors.AirlineProcessor;
import be.bstorm.formation.orsys2023uclbatch.jobs.writers.JpaItemWriter;
import be.bstorm.formation.orsys2023uclbatch.jobs.writers.JpaItemWriterBuilder;
import be.bstorm.formation.orsys2023uclbatch.jobs.writers.JpaRepositoryWriter;
import be.bstorm.formation.orsys2023uclbatch.models.AirlineRead;
import be.bstorm.formation.orsys2023uclbatch.models.AirlineWithCountry;
import be.bstorm.formation.orsys2023uclbatch.models.entities.Airline;
import be.bstorm.formation.orsys2023uclbatch.models.entities.Country;
import be.bstorm.formation.orsys2023uclbatch.repositories.AirlineRepository;
import be.bstorm.formation.orsys2023uclbatch.repositories.CountryRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.job.flow.support.SimpleFlow;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class AirportImportJob {

    @Bean
    @Qualifier("airportJob")
    public Job airportJob(
            @Qualifier("airportFlow") Flow flow,
            JobRepository jobRepository
    ) {
        return new JobBuilder("airportJob", jobRepository)
                .incrementer(new RunIdIncrementer())
                .preventRestart()
                .start(flow)
                .end()
                .build();
    }

    @Bean
    public Flow airportFlow(
            @Qualifier("transformStep") Step step,
            JobRepository jobRepository
    ) {
        return new FlowBuilder<SimpleFlow>("airportFlow")
                .start(step)
                .build();
    }

    @Bean
    @Qualifier("transformStep")
    public Step transformStep(
            PlatformTransactionManager transactionManager,
            JobRepository jobRepository,
            ItemReader<AirlineRead> reader,
            ItemProcessor<AirlineRead, AirlineWithCountry> processor,
            ItemWriter<AirlineWithCountry> writer
        ) {
        return new StepBuilder("transformStep", jobRepository)
                .<AirlineRead, AirlineWithCountry> chunk(10, transactionManager)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .build();
    }

    @Bean
    @Qualifier("airlineReader")
    public FlatFileItemReader<AirlineRead> airlineReader(

    ) {
        return new FlatFileItemReaderBuilder<AirlineRead>()
                .name("airlineReader")
                .resource(new ClassPathResource("csv/airlines.csv"))
                .delimited()
                    .delimiter("\t")
                .names("AirlineId", "AirlineName", "Alias", "IATACode", "ICAOCode", "CallSign", "Country", "Active")
                .fieldSetMapper(new BeanWrapperFieldSetMapper<>(){{
                    setTargetType(AirlineRead.class);
                }})
                .linesToSkip(1)
                .build();
    }

    @Bean
    public AirlineProcessor airlineProcessor() {
        return new AirlineProcessor();
    }

    @Bean
    public JpaItemWriter<AirlineWithCountry> jpaItemWriter(
            AirlineRepository airlineRepository,
            CountryRepository countryRepository
    ) {
        return new JpaItemWriterBuilder<AirlineWithCountry>()
                .action((awc) -> {
                    Country c = countryRepository.save(awc.getCountry());
                    awc.getAirline().setCountry(c);
                    airlineRepository.save(awc.getAirline());
                })
                .build();
    }

    public JpaRepositoryWriter<Airline, AirlineRepository> jpaRepo(
            AirlineRepository airlineRepository
    ) {
        return new JpaRepositoryWriter<Airline, AirlineRepository>(airlineRepository);
    }
}
