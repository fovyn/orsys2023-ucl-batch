package be.bstorm.formation.orsys2023uclbatch.models.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Airline {

    @Id
    private Long id;
    private String name;
    private String alias;
    private String IATACode;
    @ManyToOne
    private Country country;
    private boolean isActive;
}
