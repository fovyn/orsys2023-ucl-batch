package be.bstorm.formation.orsys2023uclbatch;

import be.bstorm.formation.orsys2023uclbatch.jobs.AirportImportJob;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Orsys2023UclbatchApplication implements CommandLineRunner {
    private final JobLauncher jobLauncher;
    private final Job airportJob;

    public Orsys2023UclbatchApplication(
            JobLauncher jobLauncher,
            @Qualifier("airportJob") Job airportJob
    ) {
        this.jobLauncher = jobLauncher;
        this.airportJob = airportJob;
    }

    public static void main(String[] args) {
        SpringApplication.run(Orsys2023UclbatchApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        jobLauncher.run(this.airportJob, new JobParameters());
    }
}
