package be.bstorm.formation.orsys2023uclbatch.jobs.writers;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.function.Consumer;
import java.util.function.Function;

public class JpaItemWriterBuilder<T> {
    private Consumer<T> action;

    public JpaItemWriterBuilder<T> action(Consumer<T> action) {
        this.action = action;
        return this;
    }

    public JpaItemWriter<T> build() {
        return new JpaItemWriter<T>(this.action);
    }
}
